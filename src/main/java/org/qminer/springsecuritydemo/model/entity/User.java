package org.qminer.springsecuritydemo.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by QwQ on 2019/10/14 14:04
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

    private static final long serialVersionUID = -4544186752528930654L;

    private Long id;
    /*
     * 用户名
     * */
    private String username;
    /*
     * 密码
     * */
    @JsonIgnore
    private String password;
    /*
     * 名称
     * */
    private String nickname;
    /*
     * 年龄
     * */
    private Long age;
    /*
     * 邮箱
     * */
    private String email;
    /*
     * 删除
     * */
    private Long deleted;
    /*
     * 创建时间
     * */
    private Date createTime;
    /*
     * 更新时间
     * */
    private Date updateTime;
    /*
     * 过期时间
     * */
    private Date expireTime;

}
