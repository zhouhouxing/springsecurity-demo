package org.qminer.springsecuritydemo.mapper;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.qminer.springsecuritydemo.model.entity.User;
import org.springframework.stereotype.Repository;

/**
 * Created by QwQ on 2019/10/14 14:08
 */
@Repository
public interface UserMapper {

    @Select(value = "select * from user where id = #{id}")
    User getById(Long id);

    @Select(value = "select * from user where username = #{username}")
    User getByUsername(String username);

    @Update(value = "update user set password = #{password} where username = #{username}")
    void save(User user);
}
