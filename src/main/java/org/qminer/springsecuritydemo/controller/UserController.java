package org.qminer.springsecuritydemo.controller;

import io.swagger.annotations.*;
import org.qminer.springsecuritydemo.mapper.UserMapper;
import org.qminer.springsecuritydemo.model.common.DataType;
import org.qminer.springsecuritydemo.model.common.ParamType;
import org.qminer.springsecuritydemo.model.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "用户控制器")
@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserMapper userMapper;

    @ApiOperation(value = "查找指定用户")
    @PostMapping(value = "/{id}")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "用户id", dataType = DataType.LONG, paramType = ParamType.PATH)})
    public User getById(@PathVariable Long id) {
        return userMapper.getById(id);
    }

}
