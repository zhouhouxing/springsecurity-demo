CREATE TABLE `permission` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`permission_name` VARCHAR(255) NOT NULL,
	`permission_code` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=3
;
